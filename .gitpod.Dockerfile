FROM gitpod/workspace-full:2022-10-30-18-48-35
ARG TERRAFORM_VERSION=1.3.3
ARG VAULT_VERSION=1.9.2
ARG KUBECTL_VERSION=v1.22.2
USER root
# Install Terraform and VAULT
RUN apt-get update -y && \
wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
mv terraform /usr/bin && \
rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
# Install Vault
wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
unzip vault_${VAULT_VERSION}_linux_amd64.zip && \
mv vault /usr/bin && \
rm vault_${VAULT_VERSION}_linux_amd64.zip && \
# Install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
chmod +x ./kubectl && \
mv ./kubectl /usr/local/bin/kubectl && \
